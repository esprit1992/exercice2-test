module.exports = class Bots {
    constructor(parms) {
        this.name = parms.name;
        this.actions = {
            PAYLOAD_GET_STARTED : {
                title : parms.actions.PAYLOAD_GET_STARTED.title,
                type  : parms.actions.PAYLOAD_GET_STARTED.type
            },
            PAYLOAD_CLIENT : {
                title : parms.actions.PAYLOAD_CLIENT.title,
                type : parms.actions.PAYLOAD_CLIENT.type
            }
        }
        this.users = parms.users;
    }
};