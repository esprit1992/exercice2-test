'use strict';

// check type isString
var checkString = exports.checkString = function (text) {
    if (typeof (text) === 'string') {
        return true;
    } else {
        return false;
    }
};

// check type is Array
exports.checkArrOfString = function (arr) {
    if (arr.constructor === Array) {
        for (let i = 0; i < arr.length; i++) {
            if (!checkString(arr[i])) {
                return false;
            }
        }
        return true;
    } else return false;
};
