'use strict';

const BotsModel = require('../models/Bots');
const validate = require('./validate');

exports.addNewBot = function (req, res, next) {
    try {
        if (req.body.hasOwnProperty('name')) {
            if (!validate.checkString(req.body.name))
                return res.status(422).send({ codeHTTP: 422, error: 'invalid name' });
        } else return res.status(422).send({ codeHTTP: 422, error: 'name required' });

        if (req.body.hasOwnProperty('actions')) {

            if (req.body.actions.hasOwnProperty('PAYLOAD_GET_STARTED')) {
                if (req.body.actions.PAYLOAD_GET_STARTED.hasOwnProperty('title')) {
                    if (!validate.checkString(req.body.actions.PAYLOAD_GET_STARTED.title))
                        return res.status(422).send({ codeHTTP: 422, error: 'invalid actions.PAYLOAD_GET_STARTED.title' });
                } else return res.status(422).send({ codeHTTP: 422, error: 'actions.PAYLOAD_GET_STARTED.title required' });

                if (req.body.actions.PAYLOAD_GET_STARTED.hasOwnProperty('type')) {
                    if (!validate.checkString(req.body.actions.PAYLOAD_GET_STARTED.type))
                        return res.status(422).send({ codeHTTP: 422, error: 'invalid actions.PAYLOAD_GET_STARTED.type' });
                } else return res.status(422).send({ codeHTTP: 422, error: 'actions.PAYLOAD_GET_STARTED.type required' });
            } else return res.status(422).send({ codeHTTP: 422, error: 'actions.PAYLOAD_GET_STARTED required' });


            if (req.body.actions.hasOwnProperty('PAYLOAD_CLIENT')) {
                if (req.body.actions.PAYLOAD_CLIENT.hasOwnProperty('title')) {
                    if (!validate.checkString(req.body.actions.PAYLOAD_CLIENT.title))
                        return res.status(422).send({ codeHTTP: 422, error: 'invalid actions.PAYLOAD_CLIENT.title' });
                } else return res.status(422).send({ codeHTTP: 422, error: 'actions.PAYLOAD_CLIENT.title required' });

                if (req.body.actions.PAYLOAD_CLIENT.hasOwnProperty('type')) {
                    if (!validate.checkString(req.body.actions.PAYLOAD_CLIENT.type))
                        return res.status(422).send({ codeHTTP: 422, error: 'invalid actions.PAYLOAD_CLIENT.type' });
                } else return res.status(422).send({ codeHTTP: 422, error: 'actions.PAYLOAD_CLIENT.type required' });
            } else return res.status(422).send({ codeHTTP: 422, error: 'actions.PAYLOAD_CLIENT required' });

        } else return res.status(422).send({ codeHTTP: 422, error: 'actions required' });

        if (req.body.hasOwnProperty('users')) {
            if (!validate.checkArrOfString(req.body.users))
                return res.status(422).send({ codeHTTP: 422, error: 'invalid users' });
        } else return res.status(422).send({ codeHTTP: 422, error: 'users required' });

        let bot = new BotsModel(req.body);
        let id = req.db.firestore().collection("bots").doc().id;
        req.db.firestore().collection('bots').doc(id).set(JSON.parse(JSON.stringify(bot)))
            .then(() => {
                return res.status(201).send({ codeHTTP: 201, msg: "insert new bot with id: " + id });
            })
            .catch(() => {
                return res.status(500).send({ codeHTTP: 500, error: "error insert new bot" });
            });

    } catch (err) {
        return res.status(422).send({ codeHTTP: 422, error: "catch" });
    };
};
//TODO ADD CHECK OF USER ID
exports.addUserToBot = function (req, res, next) {
    try {
        let botId = req.params.idBot;
        let userId = req.params.iduser;
        req.db.firestore().collection('bots').doc(botId).get()
            .then((doc) => {
                if (doc.exists) {
                    let bot = doc.data();
                    if (bot.users.find((element) => { return element === userId })) {
                        return res.status(500).send({ codeHTTP: 500, error: "userId " + userId + " already exsist inside bot " + botId + " users!" });
                    } else {
                        bot.users.push(userId);
                        req.db.firestore().collection('bots').doc(botId).update(bot)
                            .then(() => {
                                return res.status(202).send({ codeHTTP: 202, msg: "bot with id: " + botId + " was updated successfully" });
                            })
                            .catch((err) => {
                                return res.status(500).send({ codeHTTP: 500, error: "error update bot with id " + botId });
                            });
                    }
                } else {
                    return res.status(404).send({ codeHTTP: 202, msg: "bot with id: " + botId + " not found" });
                }
            })
            .catch((err) => {
                return res.status(500).send({ codeHTTP: 500, error: "error find bot with id: " + botId });
            })

    } catch (error) {
        return res.status(422).send({ codeHTTP: 422, err: "catch" });
    };
};

exports.removeBot = function (req, res, next) {
    try {
        let botId = req.params.idBot;
        req.db.firestore().collection('bots').doc(botId).get()
            .then((doc) => {
                if (doc.exists) {
                    req.db.firestore().collection('bots').doc(botId).delete()
                        .then(() => {
                            return res.status(202).send({ codeHTTP: 202, msg: "bot with id: " + botId + " was deleted successfully" });
                        })
                        .catch((err) => {
                            return res.status(500).send({ codeHTTP: 500, error: "error delete bot with id " + botId });
                        });
                } else {
                    return res.status(404).send({ codeHTTP: 202, msg: "bot with id: " + botId + " not found" });
                }
            })
            .catch(() => {
                return res.status(500).send({ codeHTTP: 500, error: "error find bot with id: " + botId });
            })

    } catch (error) {
        return res.status(422).send({ codeHTTP: 422, test: "catch error" });
    };
};