const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const Bots = require('../controllers/bots');


router.post("/", jsonParser, Bots.addNewBot);
router.patch("/:idBot/:iduser", jsonParser, Bots.addUserToBot);
router.delete("/:idBot", jsonParser, Bots.removeBot);

module.exports = router;