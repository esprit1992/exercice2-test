require('dotenv').config({ path: './config/.env' });
const express = require('express');
const app = express();
const morgan = require('morgan');
const routes = require('./routes/routes.js');
app.use(morgan('combined'));
const firebase = require('firebase').initializeApp(
    {
        apiKey: "AIzaSyC7RIL1FcjC-SXVitI01MblGzQe-JeG6hY",
        authDomain: "exercice2demotest.firebaseapp.com",
        databaseURL: "https://exercice2demotest.firebaseio.com",
        projectId: "exercice2demotest",
        storageBucket: "exercice2demotest.appspot.com",
        messagingSenderId: "643825972618",
        appId: "1:643825972618:web:838c6464ba169f6a278473"
    }
);
app.use(function (req, res, next) {
    req.db = firebase;
    next();
});

app.set('port', (process.env.APP_PORT || 8080));
routes(app);

app.listen(app.get('port'), function () {
    console.log('Express server listening on port', app.get('port'));
});

