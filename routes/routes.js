'use strict';
const cors = require('cors');
var corsOptions = {
  origin: '*'
};
module.exports = function (app) {
  app.use(cors(corsOptions));
  const botsRoutes = require('./botes_routes');
  app.get('/ping', (req, res) => {
    res.send('pong from bots API')
  });
  app.use('/bots', botsRoutes);
  app.all('*', function (req, res) {
    return res.status(404).send({ codeHTTP: 404, test: "Not Found" });
  });
};